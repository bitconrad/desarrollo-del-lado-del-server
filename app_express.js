var express = require('express');//incorpora modulo express
var app = express();//creamos la app servidor que posee muchos metodos
app.get('/', function(req, res) {//accedo con get a ruta raiz (/) luego un callback (func que se ejecuta dentro del metodo)
res.send('Hola Mundo!');//resuelve enviar hola mundo con el metodo send aplicado a res
});
app.listen(3000, function() {//define y crea el servidor escuchando puerto 3000
console.log('Aplicación ejemplo, escuchando el puerto 3000!');//imprime comentario en consola
});
const http = require('http');

const hostname = '127.0.0.1';//IP servidor
const port = 3000; //puerto por el cual vamos a escuchar

const server = http.createServer((req, res) => {//Usamos una api del mod http. l metodo recibe 2 parametros request(peticion por el peurto) y response. El callback me va a ejecutar una funciojn 
    res.statusCode = 200; //creo una instancia del servidor q atiende peticiones
    res.setHeader('Content-Type', 'text/plain');
    res.end('Hola Mundo\n');
});

server.listen(port, hostname, () => { //ponemos a escuchar el servidor, le pasamos el port y hostname y luego se ejecuta el callback
    console.log(`Servidor esta corriendo sobre http://${hostname}:${port}`);//El string tiene comandos de esc para poner variables (uso tilde invertida)
});